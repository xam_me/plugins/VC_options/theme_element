<?php 


function agency_theme_support(){
	
	
	
	add_theme_support( 'custom-logo' );
	
	
	function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );
	
	/* For the Featured Images
	===============================================================================*/
	add_theme_support('post-thumbnails' );
	/* For the Title tag
	===============================================================================*/	
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'custom-header' );
	add_editor_style( $stylesheet = 'editor-style.css' );
	/* Switch default core markup for search form, comment form, and comments to output valid HTML5.
	===============================================================================*/
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	if ( ! isset( $content_width ) ) {
			$content_width = 1920;
		}
	/* Enable support for Post Formats.
	===============================================================================*/
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );	
	/* Default Thumbnail
	===============================================================================*/
	add_image_size( 'lawyer-practice-thumb', 360, 180, true );
	add_image_size( 'lawyer-latestnews-thumb', 270, 217, true );
	add_image_size( 'practice_twoimg', 560, 300, true );
	add_image_size( 'single', 560, 300, true );
	
	/* Makes wci textdomain for translation
	===============================================================================*/
	load_theme_textdomain( 'lawyer', get_template_directory() . '/languages' );
	/*  Add default posts and comments RSS feed links to head.
	===============================================================================*/
	add_theme_support( 'automatic-feed-links' );	
	/* This theme uses wp_nav_menu()
	===============================================================================*/
	register_nav_menus( array(
		'header_menu' => 'Header Menu',
		'footer_menu' => 'Footer Menu',
	));	
	
	
}
add_action('after_setup_theme','agency_theme_support');

