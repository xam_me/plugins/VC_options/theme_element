<?php
/*
Plugin Name: Theme-elements
Plugin URI: https://gitlab.com/xam_me/plugins/VC_options/theme_element/raw/master/theme-element.zip
Description: A theme shortcode plugin
Author: Md Shahinur Islam
Version: 1.0.0
Author URI: https://gitlab.com/shahinurislam
*/


	/** ---------------------------------------------|
	/* Begin: HOME 2 About					 			 |
	/** ---------------------------------------------*/

add_shortcode('about_section', 'lawyer_about_section');
	function lawyer_about_section($attr, $content){
		extract(shortcode_atts(array(
			
			'about_title'				=> 'Title',
			'about_des'					=> 'about_des',
			
			//About Group--
			
			'about_group' 				=> '',
			'about_g_icon' 				=> '',
			'about_g_title' 			=> '',
			'about_g_des' 				=> '',
			
			
			//About Second--
			
			'about_title' 				=> 'Separator',
			
			'about_se_title' 			=> '',
			'about_se_des' 				=> '',
			'about_se_team' 			=> '',
			//About Group Second--
			'about_group_second' 		=> '',
			'about_g_se_title' 			=> '',
			'about_g_se_des' 			=> '',
			
			
			
		), $attr));
		ob_start(); ?>
		
		<!-- about-four Start -->
		<div id="about-four">
			<div class="container">
				<div class="sec-title ab4 text-center">
					<h3><?php echo esc_html($about_title);?></h3>
					<p><?php echo esc_html($about_des);?></p>
					<hr class="hr">
				</div>
				
				<div class="ab4-box">
					<div class="row">
					
					<?php  $about_group = vc_param_group_parse_atts($about_group);
							if($about_group):
								foreach($about_group as $about_value):
					?>
					
						<div class="col-md-3 col-sm-6">
							<div class="ab4-txt text-center wow fadeInUp" data-wow-delay=".1s">
								<div class="ab4-icon">
									<div class="glyph-icon <?php echo esc_attr__($about_value['about_g_icon']);?>"></div>
								</div>
								<div class="ab4-text">
									<h4><?php echo esc_html__($about_value['about_g_title']);?></h4>
									<p><?php echo esc_html__($about_value['about_g_des']);?></p>
								</div>
							</div>
						</div>
						<?php endforeach; endif;?>
					</div>
				</div>
				
				<div class="ab4-choose">
					<div class="row">
						<div class="col-md-6">
							<div class="sec-title">
								<h3><?php echo esc_html__($about_se_title);?></h3>
							</div>
							<p><?php echo esc_html__($about_se_des);?></p>
						</div>
						<div class="col-md-6">
							<div class="sec-title">
								<h3><?php echo esc_html__($about_se_team);?></h3>
							</div>
							<div class="panel-group ab4-accor" id="accordion">
							<?php $accor = vc_param_group_parse_atts($about_group_second);
									$i = 0;
										foreach($accor as $accor_value): 
									$i++;	
									?>
							
							  <div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>">
										<span class="glyphicon glyphicon-plus pull-left"></span>
									  <?php echo esc_html__($accor_value['about_g_se_title']);?>
									</a>
								  </h4>
								</div>
								<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse">
								  <div class="panel-body">
										<?php echo esc_html__($accor_value['about_g_se_des']);?>
								  </div>
								</div>
							  </div>
							  	<?php endforeach;?>
							  
							</div><!-- /#accordion -->
						</div><!-- /.col-md-6 -->
					</div><!-- /.row -->
				</div><!-- /.ab4-choose -->
			</div><!-- /.container -->
		</div><!-- /#about-four -->
		<!-- about-four End -->	
					
		
					
		<?php
		return ob_get_clean();
	}	
	
	/** ---------------------------------------------|
	/* Begin:HOME 2 Legal Practice Areas			 |
	/** ---------------------------------------------*/
	
add_shortcode('practice_section', 'lawyer_practice_section');
	function lawyer_practice_section($attr, $content){
		extract(shortcode_atts(array(
					'practice_title' =>'',
					'practice_post_number' =>'',
			
				), $attr)
		);
		
	ob_start(); ?>		

		<!-- legal-practice tart -->
		<div id="legal-practice">
			<div class="container">
				<div class="sec-title">
					<h3><?php echo esc_html__($practice_title);?></h3>
				</div>
				
				<div class="legal-p">
					<div class="row">
					<?php 
						$lawyer_practice = new WP_Query(array(
							'post_type'=>'lawyer_practice',
							'taxonomy'=>'practice_group',
							'posts_per_page'=>$practice_post_number,
						));
						
						if($lawyer_practice->have_posts()): while($lawyer_practice->have_posts()): $lawyer_practice->the_post();
						
					?>
					
						<div class="col-md-4 col-sm-6">
							<div class="legal-box wow fadeInUp" data-wow-delay=".1s">
								<a href="<?php the_permalink();?>"><h4><?php echo esc_html__(the_title());?></h4></a>
								<div class="legal-img"><?php if(has_post_thumbnail()){
									the_post_thumbnail('lawyer-practice-thumb');
									
								}?></div>
								<p><?php if(! has_excerpt()){
									echo esc_attr__(wp_trim_words(get_the_content(), 15, '' ));
									
								}else{
									the_excerpt();
									
								}?></p>
							</div>
						</div>
						<?php endwhile; endif;?>
						
					</div><!-- /.row -->
				</div><!-- /.legal-p -->
			</div><!-- /.container -->
		</div><!-- /#legal-practice -->
		<!-- legal-practice End -->
	
			<?php
		return ob_get_clean();
	}	
	
	
	/** ---------------------------------------------|
	/* Begin: HOME 2 TEAM							 |
	/** ---------------------------------------------*/
	
	add_shortcode('team_section', 'lawyer_team_section');
	function lawyer_team_section($attr, $content){
		extract(shortcode_atts(array(
					'team_title' =>'',
					'team_desc' =>'',
					//group
					'team_group' =>'',
					'team_member_title' =>'',
					'team_member_desig' =>'',
					'team_member_img' =>'',
					'team_member_email' =>'',
					'team_member_phone' =>'',
					//social
					'social_icon_1' =>'',
					'social_link_1' =>'',
					'social_icon_2' =>'',
					'social_link_2' =>'',
					'social_icon_3' =>'',
					'social_link_3' =>'',
					'social_icon_4' =>'',
					'social_link_4' =>'',
					'social_icon_5' =>'',
					'social_link_5' =>'',
			
				), $attr)
		);
		
	ob_start(); ?>		

	
	
	<!-- m-law Start -->
		<div id="m-law">
			<div class="container">
				<div class="sec-title text-center">
					<h3><?php echo esc_html($team_title);?></h3>
					<p><?php echo esc_html($team_desc);?></p>
					<hr class="hr" />
					
				</div>
				<div class="mlaw">
					<div class="row">
				<?php	$team = vc_param_group_parse_atts($team_group);
						if($team):
						foreach($team as $team_value):
					?>
					
						<div class="col-md-3 col-sm-6">
							<div class="mlaw-box wow fadeInUp" data-wow-delay=".1s">
								<div class="m-txt">
									<h5><?php echo esc_html__($team_value['team_member_title']);?></h5>
									<p><?php echo esc_html__($team_value['team_member_desig']);?></p>
								</div>
								<div class="m-img">
									<?php $mem_img = wp_get_attachment_image_src($team_value['team_member_img'], 'full');
									
										if($mem_img):
									?>
								
								<img src="<?php echo esc_url($mem_img[0]);?>" alt="Image" />
								<?php endif;?>
								
								</div>
								<p class="ml"><?php echo esc_html__($team_value['team_member_email']);?><br /><?php echo esc_html__($team_value['team_member_phone']);?></p>
								<hr />
								<ul>
									<?php if(!empty($team_value['social_link_1'])):?><li><a href="<?php echo esc_url($team_value['social_link_1']);?>"><i class="<?php echo esc_html($team_value['social_icon_1']);?>"></i></a></li>
									<?php endif;?>
									
									<?php if(!empty($team_value['social_link_2'])):?><li><a href="<?php echo esc_url($team_value['social_link_2']);?>"><i class="<?php echo esc_html($team_value['social_icon_2']);?>"></i></a></li>
									<?php endif;?>
									
									<?php if(!empty($team_value['social_link_3'])):?><li><a href="<?php echo esc_url($team_value['social_link_3']);?>"><i class="<?php echo esc_html($team_value['social_icon_3']);?>"></i></a></li>
									<?php endif;?>
									
									<?php if(!empty($team_value['social_link_4'])):?><li><a href="<?php echo esc_url($team_value['social_link_4']);?>"><i class="<?php echo esc_html($team_value['social_icon_4']);?>"></i></a></li>
									<?php endif;?>
									
									<?php if(!empty($team_value['social_link_5'])):?><li><a href="<?php echo esc_url($team_value['social_link_5']);?>"><i class="<?php echo esc_html($team_value['social_icon_5']);?>"></i></a></li>
									<?php endif;?>
									
								</ul>
							</div>
						</div>
						<?php endforeach; endif;?>
					</div><!-- /.row -->
				</div><!-- /.mlaw -->
			</div><!-- /.container -->
		</div><!-- /#m-law -->
		<!-- m-law End -->

				<?php
		return ob_get_clean();
	}	
	
	

	/** ---------------------------------------------|
	/* Begin: HOME 2 Letest news and testimonial	 |
	/** ---------------------------------------------*/
	
	add_shortcode('testimonials_section', 'lawyer_testimonial_section');
	function lawyer_testimonial_section($attr, $content){
		extract(shortcode_atts(array(
					'teastimonial_title' =>'',
					'teastimonial_des' =>'',
					'blog_page_number' =>'',
					//group
					'testimonial_group' => '',
					'teastimonial_img' => '',
					'teastimonial_img_title' => '',
					'teastimonial_img_des' => '',
					'teastimonial_img_name' => '',
					'teastimonial_img_name_desig' => '',
			
				), $attr)
		);
		
	ob_start(); ?>		

				<!-- lnwes-test Start -->
		<div id="lnwes-test">
			<div class="container">
				<div class="sec-title ab4 text-center">
					<h3><?php echo esc_html($teastimonial_title);?></h3>
					<p><?php echo esc_html($teastimonial_des);?></p>
					<hr class="hr">
				</div>
			
				<div class="ln-testi">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="row">
							<?php $latestnews =  new WP_Query(array(
								'post_type' => 'latest_news',
								'posts_per_page' => $blog_page_number,
						
							));
								
								if($latestnews->have_posts()):  while($latestnews->have_posts()): $latestnews->the_post();
							?>
							<?php $sticky = !is_sticky() ? "non-sticky" : "sticky";?>
							
							<article id="post-<?php the_ID(); ?>" <?php post_class($sticky); ?>>
							
							
								<div class="col-md-6  col-sm-6">
									<div class="ln-test">
										<div class="ln-img">
										
										<?php if(has_post_thumbnail()){
												the_post_thumbnail('lawyer-latestnews-thumb');
												
											}?>
										
										<?php //get_template_part( 'template-parts/front', 'postformat');?>
										
										<?php //$format =array('image','quote','status','chat');
											//if(has_post_format($format) || has_post_thumbnail ){
										
										?>
										
											<div class="ln-over">
												<p><?php if(!has_excerpt()){
													echo esc_html(wp_trim_words(get_the_content(), 15));
												}else{
													the_excerpt();
												}?></p>
											</div>
											<?php //}?>
										</div>
										<p class="lntxt"><?php echo esc_html(get_the_time('F'));?> <?php echo esc_html(get_the_time('j'));?>, <?php echo esc_html(get_the_time('Y'));?><span class="ln-comm pull-right"><i class="fa fa-comment-o"></i><?php comments_popup_link( __( ' 0', 'lawyer' ), __( '1 comment', 'lawyer' ), __( '% comments', 'lawyer' )); ?></span></p>
										<a href="<?php the_permalink();?>"><h4><?php esc_html(the_title());?></h4></a>
										<hr />
									</div>
								</div>
								</article>
								<?php endwhile;
									
									// If no content, include the "No posts found" template.
							else :
								get_template_part( 'template-parts/content', 'none' );
								
								endif;?>
								
								
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div id="home4test" class="owl-carousel owl-theme">
							
							<?php $testimonial = vc_param_group_parse_atts($testimonial_group);
								if($testimonial):
									foreach($testimonial as $test_value):
							?>
								
								<div class="item">
									<div class="lns-img">
									<?php $test_value_img = wp_get_attachment_image_src($test_value['teastimonial_img']);
										if($test_value_img):
									
									?>
										<img src="<?php echo esc_url($test_value_img[0]);?>" alt="Image" />
										<?php endif;?>
									</div>
									<div class="lns-text">
										<h4><?php echo esc_html('teastimonial_img_title');?></h4>
										<p><?php echo esc_html('teastimonial_img_des');?></p>
									</div>
									<div class="lns-text2">
										<h5><?php echo esc_html('teastimonial_img_name');?></h5>
										<h6><?php echo esc_html('teastimonial_img_name_desig');?></h6>
									</div>
								</div>
								<?php endforeach; endif;?>
								
							</div>
						</div><!-- /.col-md-6 -->
					</div><!-- /.row -->
				</div><!-- /.ln-testi -->
			</div><!-- /.container -->
		</div><!-- /#lnwes-test -->
		<!-- lnwes-test End -->
		
	
			
				<?php
		return ob_get_clean();
	}	

	

	/** ---------------------------------------------|
	/* Begin: HOME 2 Contact us from				 |
	/** ---------------------------------------------*/
	
	add_shortcode('contact_us_home_section', 'lawyer_homtwo_contact_us_section');
	function lawyer_homtwo_contact_us_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'home_2_contact_title' =>'',
					'home2_contact_des' =>'',
			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
	<!-- h4-contact End -->
		<div id="h4-contact">
			<div class="container">
				<div class="sec-title ab4 text-center">
					<h3><?php echo esc_html($home_2_contact_title);?></h3>
					<p><?php echo esc_html($home2_contact_des);?></p>
					<hr class="hr">
				</div>
			
					<?php 
								 include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
								 if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) { 
									echo '<div class="rquest-quote-form">' . do_shortcode($content) . '</div>';
								}
							?>
			
			
				
			</div><!-- /.container -->
		</div><!-- /#h4-contact -->
		<!-- h4-contact End -->
	
	
	
		
					<?php
		return ob_get_clean();
	}	
	 
	 

	 
	
	/** ---------------------------------------------|
	/* Begin: About us -- bredcump 					|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('bredcump_section', 'lawyer_bredcump_section');
	function lawyer_bredcump_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'about_us_m_img' =>'',
		
			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
		<!-- bg-top Start -->

		
		<div class="bg-top">
		<?php $about_usus_img = wp_get_attachment_image_src($about_us_m_img, 'full');
		?>
		
			<img src="<?php echo esc_url($about_usus_img[0]);?>" alt="images" />
		
		</div>
		<!-- bg-top End -->
		
	 
	 
	 					<?php
		return ob_get_clean();
	}	
	 

	
	/** ---------------------------------------------|
	/* Begin: About us -- About	 					|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('about_us_section', 'lawyer_about_us_section');
	function lawyer_about_us_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					
					'about_us_m_title' =>'',
					'about_us_m_heading' =>'',
					'about_us_m_des' =>'',
					'about_us_m_img_img' =>'',
					//'about_us_m_title' =>'',
			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
	
				
	
		
		<div id="ab-page">
			<div class="container">
				<div class="sec-title text-center">
					<h3><?php echo esc_html($about_us_m_title);?></h3>
				</div>
				<div class="ab-page">
					<div class="row">
						<div class="col-md-6">
							<div class="abpage-txt">
								<h5><?php echo esc_html($about_us_m_heading);?></h5>
								<p><?php echo esc_html($about_us_m_des);?></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="ab1-img">
							<?php $about_us_m_img_img = wp_get_attachment_image_src($about_us_m_img_img, 'full');
								?>
								<img src="<?php echo esc_url($about_us_m_img_img[0]);?>" alt="Image" />
							</div>
							
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	
	
	
		
					<?php
		return ob_get_clean();
	}	
	 
	 

	 
	
	/** ---------------------------------------------|
	/* Begin: About us -- OUR MISSION & VISION						|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('about_mission_section', 'lawyer_mission_section');
	function lawyer_mission_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'about_mission_title' =>'',
					'about_mission_img' =>'',
					
					//group
					'mission_group' =>'',
					'about_mission_check_title' =>'',
					'about_mission_check' =>'',
					//link
					'about_mission_submit_title' =>'',
					'about_mission_submit_link' =>'',
				
				
			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
				<div id="mission-vision">
			<div class="container">
				<div class="sec-title text-center">
					<h3><?php echo esc_html($about_mission_title);?></h3>
				</div>
				<div class="mission-vision">
					<div class="row">
						<div class="col-md-6">
						<?php $mission_iamges = wp_get_attachment_image_src($about_mission_img,'full');?>
						
							<img src="<?php echo esc_url($mission_iamges[0]);?>" alt="Image" />
							
							
						</div>
						<div class="col-md-6">
							<div class="content">
							  <div class="box">
								<ul>
								<?php 
									$all_abtmission_grp= vc_param_group_parse_atts($mission_group);
									
										foreach($all_abtmission_grp as $abtmission):
								
									if($abtmission['about_mission_check']){
										
									
								?>
									<li>
									  <input type="checkbox" checked="checked" id="c1" name="cb">
									  <label ><?php echo esc_html($abtmission['about_mission_check_title']);?></label>
									</li>
									<?php } else {?>
									
									<li>
									  <input type="checkbox" checked="checked" id="c2" name="cb">
									  <label ><?php echo esc_html($abtmission['about_mission_check_title']);?></label>
									</li>
									<?php }?>
									
									<?php endforeach;?>
								</ul>
							  </div>
							  <div class="c-btn"><a href="<?php echo esc_url($about_mission_submit_link);?>"><button class="btn btn-default"><?php echo esc_html($about_mission_submit_title);?></button></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		 	
					<?php
		return ob_get_clean();
	}	
	
	

	 
	
	/** ---------------------------------------------|
	/* Begin: About us -- Attorney Profile Section						|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('attr_profile_section', 'lawyer_attr_profile_section');
	function lawyer_attr_profile_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'attr_profile_img' =>'',
					'attr_profile_phone' =>'',
					'attr_profile_email' =>'',
					'attr_profile_title' =>'',
					'attr_profile_desig' =>'',
					'attr_profile_dgre1' =>'',
					'attr_profile_dgre2' =>'',
					'attr_profile_dgre3' =>'',
					'attr_profile_descrip' =>'',
					
					//group
					'attr_pro_group' =>'',
					'attr_pro_gro_line' =>'',
				
				
			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
	<!-- profile Start -->
		<div id="profile">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="pro-img wow fadeInLUp" data-wow-delay=".2s">
						
						<?php $attr_img = wp_get_attachment_image_src($attr_profile_img,'full');?>
						
							<img src="<?php echo esc_url($attr_img[0]);?>" alt="Image" />
							
							
							<div class="pro-txt">
								<p><i class="fa fa-phone"></i><?php echo esc_html($attr_profile_phone);?></p>
								<p><i class="fa fa-envelope"></i> <?php echo esc_html($attr_profile_email);?></p>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="pro-box">
							<h3><?php echo esc_html($attr_profile_title);?></h3>
							<h5><?php echo esc_html($attr_profile_desig);?></h5>
							<p class="ppp"><span><?php echo esc_html($attr_profile_dgre1);?></span><span><?php echo esc_html($attr_profile_dgre2);?></span><span><?php echo esc_html($attr_profile_dgre3);?></span></p>
							<p><?php echo esc_html($attr_profile_descrip);?>
							</p>
							
							<ul>
							
								<?php $attr_g = vc_param_group_parse_atts($attr_pro_group);
								if($attr_g):
									foreach($attr_g as $attr_g_value):
								?>
								<li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i><?php echo esc_html($attr_g_value['attr_pro_gro_line']);?></li>
								<?php endforeach; endif;?>
							</ul>
						</div>
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /#profile -->
		<!-- profile End -->
	
		
	
	
					<?php
		return ob_get_clean();
	}	
	 
	 

	

	 
	
	/** ---------------------------------------------|
	/* Begin: About us -- Attorney circle Section|	|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('attr_circle_section', 'lawyer_attr_circle_section');
	function lawyer_attr_circle_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'attr_cir_group'  	=>'',
					'attr_circle_line' 	=>'',
					'attr_circle_title' =>'',
					'attr_circle_des'	=>'',

			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
	<!-- circle Start -->
		<div id="circle">
			<div class="container">
				<div class="row text-center">
				
				<?php  $circle_se = vc_param_group_parse_atts($attr_cir_group);
						if($circle_se):
						$i = 0;
									foreach($circle_se as $circle_value):
									$i++;
				?>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class=" wow fadeInUp" data-wow-delay=".1s">
							<div id="circle-<?php echo $i;?>" class="myStathalf" data-dimension="180" data-text="<?php echo esc_html($circle_value['attr_circle_line']);?>%" data-type="half" data-width="8" data-fontsize="24" data-percent="<?php echo esc_html($circle_value['attr_circle_line']);?>" data-fgcolor="#ad926d" data-bgcolor="#ebebeb" data-part="65"></div>
							
							<div class="cir-txt">
								<h5><?php echo esc_html($circle_value['attr_circle_title']);?></h5>
								<p><?php echo esc_html($circle_value['attr_circle_des']);?></p>
							</div>
						</div>
					</div>
					
					<?php endforeach; endif;?>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /#circle -->
		<!-- circle End -->
		
	
	
	 	 
	 			<?php
		return ob_get_clean();
	}	
	

	

	 
	
	/** ---------------------------------------------|
	/* Begin: About us -- Attorney Xprofile Section|	|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('attr_xpro_section', 'lawyer_attr_xpro_section');
	function lawyer_attr_xpro_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'attr_xpro_title'  	=>'',
					'attr_xpro_des' 	=>'',
					'attr_xpro_hedding' =>'',
					//
					'attr_xpro_g_group'	=>'',
					'attr_xpro_g_name'	=>'',
					'attr_xpro_g_desig'	=>'',
					'attr_xpro_g_Image'	=>'',
					'attr_xpro_g_link'	=>'',
				

			
			
				), $attr)
		);
		
	ob_start(); ?>		


	
		<!-- hre Start -->
		<div id="hre">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="hre-txt1">
							<h1><?php echo esc_html($attr_xpro_title);?></h1>
							<p><?php echo esc_html($attr_xpro_des);?></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="hre-txt text-center">
							<h4><?php echo esc_html($attr_xpro_hedding);?></h4>
							<ul>
							<?php $xprofile = vc_param_group_parse_atts($attr_xpro_g_group);
								if($xprofile):
								foreach($xprofile as $xprofile_value):
							?>
								<li class=" wow fadeInLUp" data-wow-delay=".1s">
									<div class="hre-img">
									
							<?php	$xpro = wp_get_attachment_image_src($xprofile_value['attr_xpro_g_Image'],'full');?>
							<?php if($xpro):?>
									<img src="<?php echo esc_url($xpro[0]);?>" alt="Images" />
									
									<?php endif;?>
									
									</div>
									<h5><?php echo esc_html($xprofile_value['attr_xpro_g_name']);?></h5>
									<p><?php echo esc_html($xprofile_value['attr_xpro_g_desig']);?><a href="<?php echo esc_html($xprofile_value['attr_xpro_g_link']);?> "> | profile <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
								</li>
								<?php endforeach; endif;?>
								
							</ul>
						</div>
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /#hre -->
		<!-- hre End -->
		

		
	
					<?php
		return ob_get_clean();
	}	
	 
	 

	 
	
	/** ---------------------------------------------|
	/* Begin: About us -- Attorney Company Section|	|
	/** ---------------------------------------------*/
	
	 
	 add_shortcode('attr_company_section', 'lawyer_attr_company_section');
	function lawyer_attr_company_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'attr_com_title'  	=>'',
					//gp
					'attr_company_group' 	=>'',
					'attr_com_img' =>'',
			
					'attr_com_link'	=>'',
			

			
			
				), $attr)
		);
		
	ob_start(); ?>	


<!-- brand Start -->
		<div id="brand">
			<div class="container">
				<div class="brand text-center">
					<h3><?php echo esc_html($attr_com_title);?></h3>
					
					<ul id="owl-demo2" class="owl-carousel owl-theme">
						<?php $com_group = vc_param_group_parse_atts($attr_company_group);
								if($com_group):
								foreach($com_group as $company):
						?>
						<li>
						  <div class="item">
						  <?php 
						  
						  $com_img = wp_get_attachment_image_src($company['attr_com_img'],'full');
						  
						  ?>
							<a href="<?php echo esc_url($company['attr_com_link']);?>"><img src="<?php echo esc_url($com_img[0]);?>" alt="Image" /></a>
						  </div>
						</li>
						<?php endforeach; endif;?>
						
					</ul>
				</div><!-- /.brand -->
			</div><!-- /.container -->
		</div><!-- /#brand -->
		<!-- brand End -->
		
	 	
	 	 
	 			<?php
		return ob_get_clean();
	}	
	

	
	/** ---------------------------------------------|
	/* Begin: Contact 2 Contact us from	 |
	/** ---------------------------------------------*/
	
	add_shortcode('contact_us_two_section', 'lawyer_contact_two_section');
	function lawyer_contact_two_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'contact_2_title' =>'',
					
					'contact_phone_title' =>'',
			
				
					'contact_1_social_link' =>'',
					'contact_2_social_link' =>'',
					'contact_3_social_link' =>'',
					'contact_4_social_link' =>'',
					
			
				), $attr)
		);
		
	ob_start(); ?>		

	
		
		<!-- p-head Start -->
		<div id="p-head" class="con-p1 contact2 text-center">
			<h3><?php echo esc_html($contact_2_title);?></h3>
			<hr />
			<div class="container">
				<div class="row">
						<?php 
								 include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
								 if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) { 
									echo '<div class="rquest-quote-form">' . do_shortcode($content) . '</div>';
								}
							?>
				</div>
			</div>
			
			
			
			
				
			
			
		</div>
		<!-- p-head End -->
		
		<div id="contact2">
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					<div class=" col-md-8 text-center">
						<p class="c2-top">DO NOT LIKE FORMS? THAT IS OK! WE HAVE SEVERAL WAYS TO GET IN TOUCH WITH US.</p>
						<div class="pullleft pull-left">
							<p>Give Us a Call</p>
							<h4><?php echo esc_html($contact_phone_title);?></h4>
						</div>
						<div class="pullright pull-right">
							<p>Or, Lets Get Social</p>
							<div>

								<a href="<?php echo esc_html($contact_1_social_link);?>"><i class="fa fa-skype"></i></a>
								<a href="<?php echo esc_html($contact_2_social_link);?>"><i class="fa fa-facebook-square"></i></a>
								<a href="<?php echo esc_html($contact_3_social_link);?>"><i class="fa fa-twitter-square"></i></a>
								<a href="<?php echo esc_html($contact_4_social_link);?>"><i class="fa fa-linkedin-square"></i></a>
								
								
							</div>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
		</div>
	
	
		
					<?php
		return ob_get_clean();
	}	
	 
	 
	 
	 

	
	/** ---------------------------------------------|
	/* Begin: Contact 2 Contact us from	 |
	/** ---------------------------------------------*/
	
	add_shortcode('practice_two_section', 'lawyer_practice_two_section');
	function lawyer_practice_two_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'practice_page_number' =>'',
					
			
			
				), $attr)
		);
		
	ob_start(); ?>		

	
	 
	 
	 <!-- practice2 Start -->
		<div id="practice2">
			<div class="container">
				<div class="row">
				<?php $pactice_two = new WP_Query(array(
						'post_type'=>'practice_two',
						'taxonomy'=>'Practice_two_tx',
						'posts_per_page'=>$practice_page_number,
				
				));
				
			
							if($pactice_two->have_posts())	: while($pactice_two->have_posts())	: $pactice_two->the_post();
				
				
				?>
				
					<div class="col-md-12 col-sm-12">
						<div class="prac-box2 wow fadeInUp" data-wow-delay=".1s">
							<div class="row">
								<div class="prac-img2 col-md-6 col-sm-6">
								
								<?php if(has_post_thumbnail()){
									the_post_thumbnail('practice_twoimg');
									
								}?>
								
								</div>
								<div class="prac-txt2  col-md-6 col-sm-6">
									<a href="<?php the_permalink();?>"><h3><?php echo esc_html(the_title());?></h3></a>
									<p><?php echo esc_attr(wp_trim_words(get_the_content(), 20 , '....'));?></p>
									<a href="<?php the_permalink();?>" class="btn btn-default">Read More</a>
								</div>
							</div>
						</div>
					</div>
					
						<?php endwhile; endif;?>
							
					
				</div>
			</div><!-- /.container -->
		</div><!-- /#practice2 -->
		<!-- practice2 End -->
		
	 
	 
	 
	 
		 
	 
	 	 
	 			<?php
		return ob_get_clean();
	}	
	
 
	 

	 

	
	/** ---------------------------------------------|
	/* Begin: Contact 2 Contact us from	 |
	/** ---------------------------------------------*/
	
	add_shortcode('practice_two_re_section', 'lawyer_practice_two_re_section');
	function lawyer_practice_two_re_section($attr, $content){
		extract(shortcode_atts(array(
					//'content' =>'',
					'practice_two_re_bg' =>'',
					'practice_two_re_title' =>'',
					'practice_two_re_num' =>'',
					'practice_two_re_des' =>'',
					// sec
					'practice_two_re_sec_bg' =>'',
					'practice_two_re_se_title' =>'',
					'practice_two_re_se_des' =>'',
					'practice_two_re_se_link' =>'',
					
			
			
				), $attr)
		);
		
	ob_start(); ?>		

		 
	 
		<!-- referal Start -->
		<div id="referal">
			<div class="row text-center">
				<div class="col-md-6 no-padding">
					<div class="refer">
					<?php $re_prac = wp_get_attachment_image_src($practice_two_re_bg,'full');?>
						<img src="<?php echo esc_url($re_prac[0]);?>" alt="Image" />
						
						<div class="ref-overlay"></div>
						<div class="refer-txt">
							<h2><span><?php echo esc_html($practice_two_re_num);?></span> <?php echo esc_html($practice_two_re_title);?></h2>
							<h4><?php echo esc_html($practice_two_re_des);?></h4>
						</div>
					</div>
				</div>
				<div class="col-md-6 no-padding">
					<div class="refer">
						<?php $re_prac_se = wp_get_attachment_image_src($practice_two_re_sec_bg,'full');?>
						<img src="<?php echo esc_url($re_prac_se[0]);?>" alt="Image" />
						<div class="ref1-overlay"></div>
						<div class="refer-txt1">
							<h2><?php echo esc_html($practice_two_re_se_title);?></h2>
							<hr />
							<p><?php echo esc_html($practice_two_re_se_des);?></p>
							<a href="<?php echo esc_html($practice_two_re_se_link);?>" class="btn btn-default">JOIN WITH US</a>
						</div>
					</div>
				</div><!-- /.col-md-6 -->
			</div><!-- /.row -->
		</div><!-- /#referal -->
		<!-- referal End -->
		
	 
	 
	 	 
		
					<?php
		return ob_get_clean();
	}	
	 
	 
	 
	 
	 
	 
	 
	 

